---
title: "Publications"
# meta title
meta_title: ""
# meta description
description: "Key publications for IDP-Z3."
# save as draft
draft: false
---

This page lists a few key publications about FO(·), IDP-Z3 and related tools.

### About the language FO(·) and its semantics

* Denecker, M. (2000, July). [Extending classical logic with inductive definitions.](https://people.cs.kuleuven.be/~marc.denecker/A-PDF/CL-2000.pdf) In International Conference on Computational Logic (pp. 703-717). Berlin, Heidelberg: Springer Berlin Heidelberg. **🏆 20-Year Test of Time Award at ICLP 2020 🏆**
* Denecker, M., Pelov, N., & Bruynooghe, M. (2001). [Ultimate well-founded and stable semantics for logic programs with aggregates.](https://www.researchgate.net/publication/2378200_Ultimate_Well-founded_and_Stable_Semantics_for_Logic_Programs_with_Aggregates) In Logic Programming: 17thInternational Conference, ICLP 2001 Paphos, Cyprus, November 26–December 1, 2001 Proceedings 17 (pp. 212-226). Springer Berlin Heidelberg. **🏆 20-Year Test of Time award at ICLP 2021 🏆**
* Vennekens, J., Denecker, M., & Bruynooghe, M. (2010). [FO(ID) as an extension of DL with rules.](https://lirias.kuleuven.be/retrieve/126883) Annals of Mathematics and Artificial Intelligence, 58(1), 85–115.

### About the inference systems and algorithms

* Carbonnelle, P., Vandevelde, S., Vennekens, J., & Denecker, M. (2022). [IDP-Z3: A reasoning engine for FO (.).](https://arxiv.org/pdf/2202.00343)
* Carbonnelle, P., Aerts, B., Deryck, M., Vennekens, J., & Denecker, M. (2019). [An interactive consultant.](http://ceur-ws.org/Vol-2491/demo45.pdf) Proceedings of the 31st benelux conference on artificial intelligence (BNAIC 2019) and the 28th belgian dutch conference on machine learning (benelearn 2019), brussels, belgium, november 6-8, 2019 (Vol. 2491). CEUR-WS.org.
* De Cat, B., Bogaerts, B., Bruynooghe, M., Janssens, G., & Denecker, M. (2018). [Predicate logic as a modeling language: The IDP system.](https://doi.org/10.1145/3191315.3191321) In M. Kifer & Y. A. Liu (Eds.), Declarative Logic Programming: Theory, Systems, and Applications (pp. 279–323). ACM.
* Denecker, M., & Vennekens, J. (2008). [Building a Knowledge Base System for an Integration of Logic Programming and Classical Logic.](https://doi.org/10.1007/978-3-540-89982-2_12) In M. Garcia de la Banda & E. Pontelli (Eds.), Logic Programming (Vol. 5366, pp. 71–76). Springer Berlin Heidelberg.

### About applications of our tools

* Callewaert, B., Decleyre, N., Vandevelde, S., Comenda, N., Coppens, B., & Vennekens, J. (2023). [Facilitating Investment Strategy Negotiations through Logic.](https://doi.org/10.1109/SSCI52147.2023.10372044) 2023 IEEE Symposium Series on Computational Intelligence (SSCI)
* Vandevelde, S., Jordens, J., Van Doninck, B., Witters, M., & Vennekens, J. (2024). [Knowledge-Based Support for Adhesive Selection: Will it Stick?](https://arxiv.org/pdf/2311.06302). Theory and Practice of Logic Programming.
* Aerts, B., Deryck, M., & Vennekens, J. (2022). [Knowledge-based decision support for machine component design: A case study.](https://doi.org/10.1016/j.eswa.2021.115869) Expert Systems with Applications, 187, 115869.
* Deryck, M., Comenda, N., Coppens, B., & Vennekens, J. (2021). [Combining logic and natural language processing to support investment management.](https://proceedings.kr.org/2021/66/kr2021-0066-deryck-et-al.pdf) Proceedings of the International Conference on Principles of Knowledge Representation and Reasoning, 18, Article 1.
* Deryck, M., Devriendt, J., Marynissen, S., & Vennekens, J. (2019). [Legislation in the knowledge base paradigm: Interactive decision enactment for registration duties.](https://lirias.kuleuven.be/retrieve/545085) Proceedings of the 13th IEEE Conference on Semantic Computing, 174–177.



### About alternative knowledge formalisms

* Callewaert, B., Vandevelde, S., Coppens, B., Comenda, N., Decleyre, N., & Vennekens, J. (2024). [Extending Feature Models with Types.](https://hdl.handle.net/10125/107010) Proceedings of the 57th Hawaii International Conference on System Sciences.
* Vandevelde, S., Callewaert, B., & Vennekens, J. (2022). [Interactive feature modeling with background knowledge for validation and configuration.](https://dl.acm.org/doi/abs/10.1145/3503229.3547039) Proceedings of the 26th ACM International Systems and Software Product Line Conference-Volume B, 209–216. **🏆 Best PhD Paper Award at ConfWS 2022 🏆**
* Vandevelde, S., Aerts, B., & Vennekens, J. (2021). [Tackling the DM challenges with cDMN: A tight integration of DMN and constraint reasoning.](https://doi.org/10.1017/S1471068421000491) Theory and Practice of Logic Programming, 1–24. **🏆 Best Paper Award at RuleML+RR 2020 🏆**
* Vandevelde, S., Etikala, V., Vanthienen, J., & Vennekens, J. (2021). [Leveraging the power of IDP with the flexibility of DMN: a multifunctional API.](https://lirias.kuleuven.be/retrieve/626591).
* Dasseville, I., Janssens, L., Janssens, G., Vanthienen, J., & Denecker, M. (2016). [Combining DMN and the knowledge base paradigm for flexible decision enactment.](https://ceur-ws.org/Vol-1620/paper3.pdf) 1620. **🏆 Winner of RuleML 2016 Challenge 🏆**

