---
title: "Learn IDP-Z3"
# meta title
meta_title: ""
# meta description
description: "Learn IDP-Z3"
# save as draft
draft: false
---

<!-- {{< toc >}} -->

# Learning Resources


If you want to learn more about IDP-Z3 and eco-system, there are a number of places that you could check out.

* [IDP-Z3 By Example](https://interactive-idp.gitlab.io/), an online tutorial designed to teach you the basics of FO(·) and IDP-Z3 through multiple interactive exercises. 
* [The FO[·] standard](https://fo-dot.readthedocs.io/en/latest/FO-dot.html), detailing the core language of IDP-Z3.
* [The IDP-Z3 docs](https://docs.idp-z3.be/en/stable/introduction.html), a reference overview of everything that's possible with our engine.
* [The cDMN docs](https://cdmn.readthedocs.io/en/stable/), detailing an alternative notation to FO(·).


{{< notice "tip" >}}
Want to get a quick overview of what IDP-Z3 has to offer first? Take 10 minutes to follow the quick demo below. :-)
{{< /notice >}}


# IDP-Z3 Quick Demo

##### Let's build an intelligent application together.

In this section, we will present a concrete problem case and show you how it can be modelled in IDP-Z3.
Afterwards, we introduce the Interactive Consultant as a tool for answering questions about the problem domain.

To start, imagine a case as follows:




<blockquote class="text-base leading-6">

#### The case

A local handyman is an expert at hanging objects on walls. Sometimes he uses a nail, sometimes he uses glue, sometimes he uses a screw. Over the years, he has gathered the following knowledge: 
* Nails support weights up to 25kg, screws support up to 40kg and glue supports only 15kg.
* Nails and screws require drilling holes in the wall, which cannot be done in tile walls.
* Glue is the easiest to use, a nail takes slightly more effort, and a screw is hardest to use.

With this knowledge, he wants to answer questions such as:
* Which method is easiest for hanging a 20kg weight on a brick wall?
* There is a nail in my wall. How much weight will it support?
* I want to hang a heavy object on my wooden wall, without knowing precisely how heavy it is. Which method will support the most weight?
* Can we hang a 25kg object on a tile wall?
* Where can I hang 20 kg?

How can the handyman leverage this knowledge?

</blockquote>

We'll enter this knowledge in an IDP-Z3 knowledge base and use the Interactive Consultant to obtain answers to these questions. 


#### The Knowledge Base

Below, you can find a formal representation of the knowledge written in FO(·), an extended version of the well-known First Order Logic.
This representation is also called a **Theory**.
Note that it consists of two blocks:

* **vocabulary block**: declares the symbols that describe the problem domain.
* **theory block**: contains the "laws of the field" written in FO(·) formulas and definitions.

Feel free to skim over the full theory and see if you can understand what it says.
If you cannot understand it yet, don't worry -- you do not need to fully understand it to be able to follow the rest of this demo.


{{< tabs >}}
{{< tab "Unicode" >}}

<pre class="text-black">
<code>
vocabulary { 
    type Method ≜ {Nail, Glue, Screw}
    type Wall ≜ {Brick, Wood, Tile}
 
    wall  : () → Wall
    method: () → Method
    hole : () → 𝔹
    weight: () → ℤ
    difficulty : () → ℤ
}
theory {
    weight() > 0.
    method() = Nail  ⇒ weight() ≤ 25.
    method() = Screw ⇒ weight() ≤ 40.
    method() = Glue  ⇒ weight() ≤ 15.

    hole() ⇔ method() = Nail ∨ method() = Screw.
    wall() = Tile ⇒ ¬hole().

    { difficulty() = 1 ← method() = Glue.
    difficulty() = 2 ← method() = Nail.
    difficulty() = 3 ← method() = Screw.}
}
</code>
</pre>

{{< /tab  >}}

{{< tab "ASCII" >}}

<pre class="text-black">
<code>
vocabulary {
    type Method := {Nail, Glue, Screw}
    type Wall := {Brick, Wood, Tile}

    wall  : () -> Wall
    method: () -> Method
    hole : () -> Bool
    weight: () -> Int
    difficulty : () -> Int
}
theory {
    weight() > 0.
    method() = Nail  => weight() =< 25.
    method() = Screw => weight() =< 40.
    method() = Glue  => weight() =< 15.

    hole() <=> method() = Nail | method() = Screw.
    wall() = Tile => ~hole().

    {
        difficulty() = 1 <- method() = Glue.
        difficulty() = 2 <- method() = Nail.
        difficulty() = 3 <- method() = Screw.
    }
}
</code>
</pre>

{{< /tab  >}}
{{< /tabs  >}}

#### The Interactive Consultant

As mentioned, the Interactive Consultant is a tool that lets you interact with the knowledge in any theory.
In short: it allows you to select / deselect value assignments for each symbol in your theory, after which it will automatically derive any consequences for the other symbols.
This ensures that you can interactively explore your problem domain, without ever being able to make any mistakes.
Additionally, it can also explain *why* it derived something, and it can also find optimal values.
Intrigued? Let's try it out!

Firstly, let's open up the above theory in the Interactive Consultant.
By clicking on the button below, the IC will open with the theory pre-loaded.
To check if the theory is loaded correctly, you can click on **"Edit"** in the top of the screen to view it.
Important to note is that the IC is fully **generic**: it automatically generates an interface for any theory which you enter!

{{< button label="To the IC!" link="https://interactive-consultant.idp-z3.be/?G4ewxghgRgrgNhATgTwAQG9UChW9QF2QAcBTVAWRPwAsQATVQHSIMA5CASzgBpUBxOGCR4BlMIhIB3AL448hUqgDqEOHCYYAQonZgA1j0Uh6PACqcSMvAWJkAIuwBmDnfELr0ARgB0XgMyXZXAkVNVQALlQACgBKVEAkwiUQwNQAWypaOgiY%2BIp0%2BmTaODIs2ITAVg3ATl3kiRJ2AHNqfBKcwBJCZLpHZzBXNGaE%2BycXOEIsGRoSEBQMatqG-GyAPlQABi9ktJp6bIBeVDZOXEAlwlQa%2BsbswBMiVAAmAFY1qw2MndRRcQlUY9O5y9QAFlW6zydBe-EERxOs3OsSuHnuWAKICK2UAK4SpYEvfZqQAUROjNiDYrs3pIHnhgqoXmYip9UAAawokGJrZKYDqDbrDZAvDyoQAJhHjnoS%2BAISKTcGyuj0Xtc%2BQKtkKsWLUBKhoQXr5ZU95agiWISTIZB0AM5EBBodDJYDsSQvEgADyIEAAdnQSHQ1lIgA" style="solid" >}}

We will now show you how to use the IC to answer each question of the use case.
You'll have to input some information each time, so it might be easier to open this page and the IC in parallel.

{{< accordion "Which method is easiest for hanging a 20kg weight on a brick wall?" >}}

- In the input field, set "weight" to 20.
- You'll see that you can no longer use Glue, but that Nail and Screw are still possible.
- Next to "difficulty", click on the downwards arrow to find the easiest option.
- "method" now shows that Nail is indeed the easiest way to attach the weight, at a difficulty rating of 2.

Extra: you might have noticed that after entering the weight, the IC also derived that we must always make a hole. To see why the IC derived this, click on the grey arrow next to it!


{{< /accordion >}}

{{< accordion "There is a nail in my wall. How much weight will it support?" >}}


- Reset the IC by clicking "Reset > Full" at the top.
- Select Nail
- Click the upwards arrow next to "weight": you'll see that the maximum weight is 25kg.

{{< /accordion >}}

{{< accordion "I want to hang a heavy object on my wooden wall, without knowing precisely how heavy it is. Which method will support the most weight?" >}}

- Reset the IC by clicking "Reset > Full" at the top.
- Select the "Wood" wall in the dropdown.
- Click the upwards arrow next to "weight": you'll see that the maximum weight is 40kg, if you choose a Screw.

{{< /accordion >}}

{{< accordion "Can we hang a 25kg object on a tile wall?" >}}

- Reset the IC by clicking "Reset > Full" at the top.
- Set the weight to 25kg.
- "Tile" wall is now no longer possible.
- Click on the cross next to "Tile" to obtain an explanation.

{{< /accordion >}}

{{< accordion "Where can I hang 20kg?" >}}

- Reset the IC by clicking "Reset > Full" at the top.
- In the input field, set "weight" to 20.
- Click open the dropdown next to wall.
- All options are still possible! :-)

{{< /accordion >}}



{{< notice "tip" >}}
You can learn more about the Interactive Consultant here: https://www.idp-z3.be/Interactive-Consultant/
{{< /notice >}}

#### Maintaining the Knowledge Base

Ah, the handyman now has additional knowledge: there is an exception to the rule about tiles, if you have an adequate diamond drillbit.
Let's update the theory accordingly!

- Add "diamond_drill_bit: () → 𝔹" to the vocabulary
- Replace the rule on line 18 by "wall() = Tile ∧ ¬diamond_drill_bit() ⇒ ¬hole()."I
- Click on "Run consultant"

The interface is now updated automatically. Try it out! Can you drill a hole in tile now?


#### The webIDE

Besides the IC, there are of course other ways to interact with IDP-Z3.
For instance, it exposes an API that can be used to reason with any theory.
The online IDE allows you to experiment with this:

{{< button label="To the IDE!" link="https://interactive-consultant.idp-z3.be/IDE?G4ewxghgRgrgNhATgTwAQG9UChW9QF2QAcBTVAWRPwAsQATVQHSIMA5CASzgBpUBxOGCR4BlMIhIB3AL448hUqgDqEOHCYYAQonZgA1j0Uh6PACqcSM7HlQSVa1AC5UACgCUqQEmESu7NwBbKlo6JzdPCkD6X1RaODIQ9y9AVg3ATl2oiRJ2AHNqfHiwwBJCKLp2ADMSnXhCRxcE1ELrLBkaEhAUDDSM7PxQgD5UAAYAOiiAmnpQgF5UNk5cQCXCG06c0MATIlQAJgBWYetRoMnUUXEJVAX0rOX3NYAWIZGIugP%2BQXnFi%2B6r1ABGbawomJIoUAK4SoPbjdxTGZqQAURKCHgcjpIdnhbKoDmZYqdUAAagFuYZRTDFMoVOCEA5fVCABMI4WNHhC%2BAISMjcMTymBKsgDutqbT9gyoSzUGzSeSGQBmXlg%2BmoKaIiSDGQyYoAZyICDQ6CiwHYkgOJAAHkQIAA7OgkOjDGRERDgC0wcSgjgm0Ja3A2qiEAD6NvYJu6fnoJDgXsNxrNzhMrlcjSi1iAA" style="solid" >}}


In the linked example, we perform "model expansion" on our handyman theory.
In essence, this will try to find as many possible solutions to the problem as possible.
Besides model expansion, there are a plethora of other inferences that can be applied to your knowledge.
For more information, see the resources listed at the top of the page.


#### Real life example

This handyman example is inspired by an IDP-Z3-based tool developed for the Flanders Make Joining and Materials Lab. The following video provides some more context.


{{< youtube  hO8k_64b_Oc >}}
