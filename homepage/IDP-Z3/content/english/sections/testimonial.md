---
enable: true
title: "What our Partners Are Saying About IDP-Z3"
description: "We collaborate with many companies to help build their IDP-based tools. Here's what they're saying:"

# Testimonials
testimonials:
  - name: "Government Partner"
    designation: ""
    avatar: "/images/avatar-sm.png"
    content: "I think it is a very good environment for providing integrated and consistent access to disparate regulations and see a lot of potential. The formalisation of knowledge is very intuitive, the supporting tools are powerful and the knowledge base seems easy to maintain by a domain expert without technical ICT knowledge."

  - name: "Flanders Make"
    designation: "Industrial designs"
    avatar: "/images/avatar-sm.png"
    content: "Our experts feel more confident in their decision-making when assisted by IDP-Z3 and the Interactive Consultant."

  - name: "Intelli-Select"
    designation: "Financial sector"
    avatar: "/images/avatar-sm.png"
    content: "We are looking forward to continue working with the professional and highly skilled team at KU Leuven. "

# don't create a separate page
_build:
  render: "never"
---
