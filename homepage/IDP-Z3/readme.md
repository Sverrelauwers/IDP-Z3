# IDP-Z3 website

This directory contains all the source files to create the IDP-Z3 website.

## Technology

The website is built using [Hugo](https://gohugo.io/), a static page builder, and the [Hugoplate theme](https://themes.gohugo.io/themes/hugoplate/).
This allows us to write mostly in Markdown, whilst also having access to [tailwindcss](https://tailwindcss.com/).

## Running/building the site

First, [install Hugo](https://gohugo.io/installation/) for your system.
Afterwards you can start the development server as follows:

```
hugo server -D
```

This is a dynamic server, meaning that it automatically applies changes and reloads the webpage. Pretty neat.

If you wish to then build the server, you can simply run

```
hugo
```

The static contents are placed in the `public` folder.

## Modify a page

If you wish to modify a page, there are two places to look:

* `content/english/` contains the markdown files describing our pages.
* `themes/hugoplate/layouts/` contains the html templates for our pages.

If you wish to add/remove a new page, you must also update the menu in `config/_default/menus.en.toml`.




